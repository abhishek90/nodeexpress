var PORT = 8080;
var express = require("express");
var app = express();
const fs = require("fs");
const path = require("path");

// For parsing application/json
app.use(express.json());

app.use(express.static(path.join(__dirname, "build")));

app.get("*", function (req, res) {
  res.sendFile(path.join(__dirname, "build", "index.html"));
});

// For parsing application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

function parseJwt(token) {
  try {
    var base64Url = token.split(".")[1];
    var base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
    var jsonPayload = decodeURIComponent(
      atob(base64)
        .split("")
        .map(function (c) {
          return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
        })
        .join("")
    );
    return JSON.stringify({ orderid: JSON.parse(jsonPayload).orderid });
  } catch (error) {
    return {};
  }
}

app.post("/signin", function (req, res) {
  const indexFile = path.resolve("build/index.html");
  if (req.body && req.body.transaction_response) {
    fs.readFile(indexFile, "utf8", (err, data) => {
      if (err) {
        const errMsg = `There is an error: ${err}`;
        console.error(errMsg);
        return res.status(500).send(errMsg);
      }
      const result = parseJwt(req.body.transaction_response);
      return res.send(
        data.replace(
          "<script>var __trans_response;</script>",
          `<script>window.__trans_response=${JSON.stringify(result)}</script>`
        )
      );
    });
  }
});

app.listen(PORT, function (err) {
  if (err) console.log(err);
  console.log("Server listening on PORT", PORT);
});
